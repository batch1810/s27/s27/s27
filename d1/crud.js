let http = require("http");
let port = 4000;

// Mock database
let directory = [
	{
		name: "Brandon",
		email: "brandon@mail.com"
	},
	{
		name: "Jobert",
		email: "jobert@mail.com"
	}
] 


http.createServer((request, response) =>{
	//GET Method
	if(request.url == "/users" && request.method == "GET"){

		response.writeHead(200, {"Content-Type":"application/json"});
		//JSON.stringify() method it will convert the desired output data to string 
		response.write(JSON.stringify(directory));

		// End the response process
		response.end();

	}

	// POST METHOD
	if(request.url == "/users" && request.method == "POST"){

		// This will act as a placeholder for the resource/data to be created later.
		let requestBody = "";

		// A stream is a sequence of data (a flow of data);
		// Data is received form the client and is processed in the "data" stream.
		// The information provided from the request object enters a sequence called "data" that will trigger the code below.
		// data step - this reads the "data" stream and process it as the request body.
		request.on("data", (data) =>{

			// console.log(data);
			// at this point, "requestBody" has the entire request body from postmant stored in it as a string.
			requestBody += data;
			// console.log(requestBody);

		})

		// response end step - only runs after the request has completely been sent
		request.on("end",() =>{
			// console.log(typeof requestBody);	

			requestBody = JSON.parse(requestBody);

			// console.log(typeof requestBody);

			// newUser will be save in the directory mock database.
			let newUser ={
				name: requestBody.name,
				email: requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {"Content-Type":"application/json"});
			response.write(JSON.stringify(newUser));

			response.end();
		})

	}

	// PUT Method
	if(request.url == "/users" && request.method == "PUT"){
		let requestBody = ""

		request.on("data", (data) => {
			requestBody += data;
		})

		request.on("end", () =>{

			// Convert the string Request body to JS Object
			requestBody = JSON.parse(requestBody);

			console.log(directory[1].email);

			// findIndex()
			let index = directory.findIndex(user => user.email == requestBody.email);
			console.log(index);
			if(index < 0){
				response.writeHead(404, {"Content-Type": "application/json"});
				response.write("User not found");
				response.end();
			}
			else{
				response.writeHead(200, {"Content-Type": "application/json"});
				response.write(JSON.stringify(directory[index]));
				response.end();
				directory[index].name = requestBody.name
			}


		})

		if(request.url == "/users" && request.method == "DELETE"){
			let requestBody = ""

			request.on("data", (data) => {
				requestBody += data;
			})

			request.on("end", () =>{

				// Convert the string Request body to JS Object
				requestBody = JSON.parse(requestBody);

				// findIndex()
				let index = directory.findIndex(user => user.email == requestBody.email);
				console.log(index);
				if(index < 0){
					response.writeHead(404, {"Content-Type": "application/json"});
					response.write("User not found");
					response.end();
				}
				else{
					response.writeHead(200, {"Content-Type": "application/json"});
					response.write(JSON.stringify(directory[index]));
					response.end();
					directory.slice(index,1)
				}


			})
		}
	}

}).listen(port);

console.log(`Server is running at localhost:${port}`);
