let http = require("http");

const port = 4000;

http.createServer((request, response) =>{
        // The "HTTP Method" of the incoming request can be accessed via the "method" property of the "request" parameter.
        // HTTP Methods: GET, POST, PUT, DELETE 

        // The method "GET" means that we will be retrieving or reading information.
        if(request.url == "/items" && request.method == "GET"){

                response.writeHead(200, {"Content-Type": "text/plain"});

                response.end("Data retrieve from the database");

        }

        // The method "POST" means that we will be adding or creating information
        if(request.url =="/items" && request.method =="POST"){
                response.writeHead(200, {"Content-Type":"text/plain"});

                response.end("Data to be sent to the database.");
        }
}).listen(port);

console.log(`Server is running at localhost:${port}`);

